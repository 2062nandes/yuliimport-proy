/**
 * imagesResize
 * Resize all JPG images to three different sizes: 200, 500, and 630 pixels
 */
const imagesResize = {
  '*.{jpg,png,jpeg,gif,webp}': [{
    upscale: true,
    percentage: false,
    width: 200,
    height: 200,
    crop: true,
    suffix: ''
  }, {
    width: 200 * 3,
    suffix: '-full'
  }],
  'slider/*.{jpg,png,jpeg,gif,webp}': [{
    suffix: ''
  }],
  'marcas/*.{jpg,png,jpeg,gif,webp}': [{
    suffix: ''
  }]

};

module.exports = imagesResize;