export const menuinicio = [
  {
    title: 'Inicio',
    href: '',
    icon: 'icon-home'
  },
  {
    title: 'Nosotros',
    href: 'nosotros.html',
    icon: 'icon-us'
  },
  {
    title: 'Contactos',
    href: 'contactos.html',
    icon: 'icon-contact'
  }
]
export const mainmenu = [
  {
    title: 'Restaurantes',
    href: 'restaurantes'
  },
  {
    title: 'Frial y fabrica de embutidos',
    href: 'frial-y-fabrica-de-embutidos'
  },
  {
    title: 'Heladerias',
    href: 'heladerias'
  },
  {
    title: 'Balanzas y selladoras',
    href: 'balanzas-y-selladoras'
  },
  {
    title: 'INOX',
    href: 'inox'
  },
  {
    title: 'Comida rápida',
    href: 'comida-rapida'
  },
  {
    title: 'Supermercados y micromercados',
    href: 'supermercados-y-micromercados'
  },
  // {
  //   title: 'Camaras frigorificas',
  //   href: 'camaras-frigorificas'
  // },
  {
    title: 'Snack',
    href: 'snack'
  }
]
